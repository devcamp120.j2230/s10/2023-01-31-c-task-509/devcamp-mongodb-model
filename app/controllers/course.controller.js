const mongoose = require("mongoose");
const courseModel = require("../models/course.model");

const createCourse = function (req, res) {
    // B1: Thu thập dữ liệu từ request
    var {
        reqTitle,
        reqDescription,
        reqStudent
    } = req.body;
    // { reqTitle: 'R30', reqDescription: 'React - Node', reqStudent: 20 }
    
    // B2: Validate dữ liệu
    if(!reqTitle) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Title required!"
        })
    }

    if(! (Number.isInteger(reqStudent) && reqStudent >= 0)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Number Student is not valid!"
        })
    }

    // B3: Gọi model thao tác với CSDL

    //B3.1: Chuẩn bị dữ liệu
    var requestCourse = {
        _id: mongoose.Types.ObjectId(),
        title: reqTitle,
        description: reqDescription,
        noStudent: reqStudent
    }

    //B3.2: Gọi Model để thêm dữ liệu
    courseModel.create(requestCourse, (err, data) => {
        if(err) {
            return res.status(500).json({
                status: "Internal server error",
                error: err.message
            })
        }

        return res.status(201).json({
            status: "Create successfully",
            newCourse: data
        })
    })
}

const getAllCourse = function (req, res) {
    // B1: Thu thập dữ liệu từ request
    // B2: Validate dữ liệu
    // B3: Gọi model thao tác với CSDL

    //B3.1: Chuẩn bị dữ liệu
    //B3.2: Gọi Model để lấy dữ liệu
    courseModel.find((err, data) => {
        if(err) {
            return res.status(500).json({
                status: "Internal server error",
                error: err.message
            })
        }

        return res.status(200).json({
            status: "Get courses successfully",
            courses: data
        })
    })
}

const getCourseById = function (req, res) {
    // B1: Thu thập dữ liệu từ request
    const courseid = req.params.courseid;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseid)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Cousrse ID is not valid!"
        })
    }

    // B3: Gọi model thao tác với CSDL

    //B3.1: Chuẩn bị dữ liệu
    //B3.2: Gọi Model để lấy dữ liệu
    courseModel.findById(courseid, (err, data) => {
        if(err) {
            return res.status(500).json({
                status: "Internal server error",
                error: err.message
            })
        }

        if(data) {
            return res.status(200).json({
                status: "Get course successfully",
                course: data
            })
        } else {
            return res.status(404).json({
                status: "Not Found"
            })
        }
    })
}

const updateCourseById = function (req, res) {
     // B1: Thu thập dữ liệu từ request
    const courseid = req.params.courseid;
    const {
        reqTitle,
        reqDescription,
        reqStudent
    } = req.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseid)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Cousrse ID is not valid!"
        })
    }

    // Khi update không truyền đầy đủ các trường thì vẫn hợp lệ
    // Khi không truyền trường nào thì trường đó sẽ có giá trị undefined
    if(reqTitle !== undefined && reqTitle === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Title is not valid!"
        })
    }

    if(reqStudent !== undefined && !( Number.isInteger(reqStudent) && reqStudent >=0)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Title is not valid!"
        })
    }

    // B3: Gọi model thao tác với CSDL

    //B3.1: Chuẩn bị dữ liệu
    var courseUpdate = {};
    if(reqTitle) courseUpdate.title = reqTitle;
    if(reqDescription) courseUpdate.description = reqDescription;
    if(reqStudent) courseUpdate.noStudent = reqStudent;

    //B3.2: Gọi Model để lấy dữ liệu
    courseModel.findByIdAndUpdate(courseid, courseUpdate, (err, data) => {
        if(err) {
            return res.status(500).json({
                status: "Internal server error",
                error: err.message
            })
        }

        return res.status(200).json({
            status: "Update course successfully",
            preCourse: data
        })
    })
}

const deleteCourseById = function (req, res) {
     // B1: Thu thập dữ liệu từ request
    const courseid = req.params.courseid;

     // B2: Validate dữ liệu
     if(!mongoose.Types.ObjectId.isValid(courseid)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Cousrse ID is not valid!"
        })
    }

    // B3: Gọi model thao tác với CSDL

    //B3.1: Chuẩn bị dữ liệu
    //B3.2: Gọi Model để lấy dữ liệu
    courseModel.findByIdAndDelete(courseid, (err) => {
        if(err) {
            return res.status(500).json({
                status: "Internal server error",
                error: err.message
            })
        }

        return res.status(204).json();
    })
}

module.exports = {
    createCourse,
    getAllCourse,
    getCourseById,
    updateCourseById,
    deleteCourseById
}